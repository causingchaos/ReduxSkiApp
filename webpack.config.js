var webpack = require("webpack");
var path = require('path');

var config = {
  iconPath: 'node_modules/react-icons'
};


module.exports = {
  entry: path.resolve(__dirname + "/src/index.js"),
  output: {
    path: path.resolve(__dirname + "/dist/assets"),
    filename: "bundle.js",
    publicPath: "assets"
  },
  devServer: {
    inline: true,
    contentBase: __dirname + '/dist',
    port: 3001
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["latest", "stage-0", "react"]
          }
        }
      },
      {
        test: /\.json$/,
        exclude: /(node_modules)/,
        loader: 'json-loader'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!autoprefixer-loader'
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader'
      },
      {
        test: /react-icons\/(.)*(.js)$/,
        loader: 'babel-loader',
        query: {
          presets: ['latest','stage-0']
        }
      }
    ]
  }
}