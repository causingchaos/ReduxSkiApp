import C from "../constants.js"
import { combineReducers } from 'redux'

export const goal = (state=10, action) =>
	(action.type === C.SET_GOAL) ?
		parseInt(action.payload) :
		state

export const skiDay = (state=null, action) =>
	(action.type === C.ADD_DAY) ?
		action.payload :
		state

export const errors = (state=[], action ) => {

	switch (action.type) {

		case C.ADD_ERROR :

			//if add error, then keep the current state array and and add new error
			return [
				...state,
				action.payload
			]

		case C.CLEAR_ERROR :
			//filter expects a callback function
			return state.filter( (message,i) => i !== action.payload )

		//if we don't know what the action.type is then simply return state
		default:
			return state
	}

}

export const allSkiDays = (state=[], action) => {

	switch(action.type) {

		case C.ADD_DAY :
			//check for matched items
			const hasDayAlready = state.some(skiDay => skiDay.date === action.payload.date)

			return (hasDayAlready) ?       //if this user already has an entry for the date
				state :                      // return state if true
				[
				...state,
				skiDay(null, action) //create the new ski day and add it.
			]

		case C.REMOVE_DAY :
			//for each skiDay in state, is supplied to the callback function.
			return state.filter(skiDay => skiDay.date !== action.payload)

		default:
			return state
	}
}

export const fetching = (state=false,action) => {

	switch(action.type) {

		case C.FETCH_RESORT_NAMES :
			return true

		case C.CANCEL_FETCHING :
			return false

		case C.CHANGE_SUGGESTIONS :
			return false

		default:
			return state
	}
}

export const suggestions = (state=[], action) => {

	switch(action.type) {

		case C.CLEAR_SUGGESTIONS :
			return []

		case C.CHANGE_SUGGESTIONS :
			return action.payload

		default :
			return state
	}
}

export default combineReducers({
	allSkiDays,
	goal,
	errors,
	resortNames: combineReducers({
		fetching,
		suggestions
	})
})











