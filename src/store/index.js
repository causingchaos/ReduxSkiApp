import C from '../constants'
import appReducer from './reducers'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'

/*
//middle ware
const consoleMessages = function(store) {
  return function(next) {
    return function(action) {

    }
  }
}
below is better with arrow functions.
*/
//MIDDLEWARE CONSOLE MESSAGES (REDUX ideology)
const consoleMessages = store => next => action => {

  let result

  console.groupCollapsed(`dispatching action => ${action.type}`)
  console.log(`ski days`, store.getState().allSkiDays.length )

  result = next(action)

  let { allSkiDays, goal, errors, resortNames } = store.getState()

  console.log(`
    ski days: ${allSkiDays.length}
    goal: ${goal}
    fetching: ${resortNames.fetching}
    suggestions: ${resortNames.suggestions}
    errors: ${errors.length}
    
  `)

  console.groupEnd()

  return result  //must return so the action gets registered for redux
}

/*
export default (initialState={}) => {
  return createStore(appReducer, initialState)
}
*/
export default (initialState={}) => {
  //return middleware returns a function, and we will send the createStore store function that we use
  //to create our stores, this will then return a NEW create store function that creates stores
  //with our consoleMessages middleware.
  //this then exports a default function that we can export for the skiCounter program.
  return applyMiddleware(thunk,consoleMessages)(createStore)(appReducer,initialState)
}