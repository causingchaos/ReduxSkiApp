import storeFactory from './store'
import { randomGoals } from "./actions";
import { suggestResortNames } from "./actions";
import './stylesheets/index.scss'
import './stylesheets/ui.scss'
import React from 'react'
import {render} from 'react-dom'
import { App } from "./components/App-ES6"
import { Router, Route, hashHistory } from 'react-router'
import { Routes} from 'react-router'
import { Whoops404 } from "./components/Whoops404";

import {
	addDay,
	setGoal,
	removeDay,
	addError,
	clearError,
	changeSuggestions,
	clearSuggestions,
} from './actions'

window.React = React

render (
	<Router history={hashHistory}>
		<Route path="/" component={App} />
		<Route path="list-days" component={App} >
			<Route path=":filter" component={App} />
		</Route>
		<Route path="add-day" component={App} />
		<Route path="*" component={Whoops404}/>
	</Router>,
	document.getElementById('react-container')
)

const store = storeFactory()

store.dispatch(
  suggestResortNames("hea")
)

store.dispatch(
	randomGoals()
)

store.dispatch(
  randomGoals()
)


store.dispatch(
  addDay("Heavenly","2016-12-22")
)

store.dispatch(
	removeDay("2016-12-22")
)

store.dispatch(
	setGoal(55)
)

store.dispatch(
	addError("Something went wrong")
)

store.dispatch(
	clearError(0)
)

store.dispatch(
	changeSuggestions(["One","Two","Three"])
)

store.dispatch(
	clearSuggestions()
)


/*import C from "./constants.js"
import storeFactory from './store/index'

const initialState = (localStorage['redux-store']) ?
  JSON.parse(localStorage['redux-store']) :
  {}

const saveState = () => {
	const state = JSON.stringify(store.getState())
	localStorage['redux-storage'] = state
}

const store = storeFactory(initialState)

store.subscribe(saveState)

store.dispatch({
	type: C.ADD_DAY,
	payload: {
		"resort": "Mt Shasta",
		"date": "2016-10-28",
		"powder": true,
		"backcountry":true
	}
})
store.dispatch({
  type: C.ADD_DAY,
  payload: {
    "resort": "Squaw Valley",
    "date": "2016-3-28",
    "powder": true,
    "backcountry":true
  }
})
store.dispatch({
  type: C.ADD_DAY,
  payload: {
    "resort": "The Canyons",
    "date": "2016-1-2",
    "powder": false,
    "backcountry":false
  }
})

/*
//import appReducer from './store/reducers.js'
//import {createStore} from 'redux'

const store = createStore(appReducer)

const unsubscribeGoalLogger = store.subscribe(
	() => console.log(`   Goal: ${store.getState().goal}`))

setInterval(() => {
	store.dispatch({
		type: C.SET_GOAL,
		payload: Math.floor(Math.random() * 100)
	})
}, 250)

setTimeout(() => {

	unsubscribeGoalLogger();

}, 3000)

/*
const initialState = (localStorage['redux-store']) ?
	JSON.parse(localStorage['redux-store']) :
	{}

const store = createStore(appReducer, initialState)

window.store = store

store.subscribe(() => {  //store new data to the Store.
	const state = JSON.stringify(store.getState())
	localStorage['redux-store'] = state
})

/*
store.subscribe(() => console.log(store.getState()))
store.dispatch({
	type: C.ADD_DAY,
	payload: {
		"resort": "Mt Shasta",
		"date": "2016-10-28",
		"powder": false,
		"backcountry": true
	}
})

store.dispatch({
	type: C.SET_GOAL,
	payload: 2
})
*/