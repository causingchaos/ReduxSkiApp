import C from './constants'
import fetch from 'isomorphic-fetch'
import {suggestions} from "./store/reducers";

export function addDay(resort, date, powder=false,backcountry=false) {

  return {
    type: C.ADD_DAY,
    payload: {resort,date,powder,backcountry}
  }
}

export const removeDay = function(date) {

  return {
    type: C.REMOVE_DAY,
    payload: date
  }
}

export const setGoal = (goal) =>
  ({
    type: C.SET_GOAL,
    payload: goal
  })

export const addError = (message) => ({
  type: C.ADD_ERROR,
  payload: message
})

export const clearError = function(index) {
  return {
    type: C.CLEAR_ERROR,
    payload: index
  }
}

export const changeSuggestions = (suggestions) =>
  ({
    type: C.CHANGE_SUGGESTIONS,
    payload: suggestions
  })

export const clearSuggestions = () => ({
  type: C.CLEAR_SUGGESTIONS
})


export const randomGoals = () => (dispatch,getState) => {

  if (!getState().resortNames.fetching) {
    dispatch({
      type: C.FETCH_RESORT_NAMES
    })

    setTimeout(() => {

      dispatch({
        type: C.CANCEL_FETCHING
      })

    }, 1500)

  }
}

//the suggestResortNames takes in a value and returns a function, and that function takes in
//dispatch and state as an argument, but because it's a thunk, but we only need the dispatch
// NOTE THIS CAN TAKE THE STATE, but we don't need it.
export const suggestResortNames = (value) => (dispatch) => {
  dispatch({
    type: C.FETCH_RESORT_NAMES
  })

  //this will use the isometric fetch library and return a promise
  fetch('http://localhost:3333/resorts/' + value)
    .then(response => response.json())
    .then(suggestions => {

      dispatch({
        type: C.CHANGE_SUGGESTIONS,
        payload: suggestions
      })
    })
    .catch(error => {
      dispatch(
        addError(error.message)
      )
      dispatch({
        type: C.CANCEL_FETCHING
      })
    })
}











