import React from 'react'
import { Router, Route, hashHistory } from 'react-router'
import Home from './components/ui/Home'
import MemberList from "./components/ui/MemberList";
import About from "./components/ui/About"
import {Left, Right, Whoops404} from "./components/index";

const routes = (
	render (
		<Router history={hashHistory}>
			<Route path="/" component={App} />
			<Route path="list-days" component={App} >
				<Route path=":filter" component={App} />
			</Route>
			<Route path="add-day" component={App} />
			<Route path="*" component={Whoops404}/>
		</Router>,
		document.getElementById('react-container')
	)
)

export default routes