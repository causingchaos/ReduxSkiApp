import FaShield from 'react-icons/lib/fa/shield'
import { Component } from 'react'

class Member extends Component {

	//component lifecycle, will fire right at our first render.
	componentWillMount(){
		this.style={
			backgroundColor: 'gray'
		}
	}

	render() {

		const { name, thumbnail, email, admin, makeAdmin} = this.props

		return (
			<div className="member" style={this.style}>

				<h1>{name} {(admin) ? <FaShield/> : null}</h1>
				<button onClick={makeAdmin}>Make Admin</button>
				<img src={thumbnail} alt="profile picture"/>
				<p><a href={`mailto:${email}`}>{email}</a></p>

			</div>
		)
	}
}

export default Member

/*
Structure of what's being passed in.
render(
	<Member admin={true}
					name="Edna Welch"
					email="edna.welch88@example.com"
					thumbnail="https://randomuser.me/api/portraits/women/90.jpg"
					makeAdmin={(email) => console.log(email)}/>,
	document.getElementById('react-container')
)
 */
