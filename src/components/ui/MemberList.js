import { Component } from 'react'
import fetch from 'isomorphic-fetch'
import Member from './Member'

class MemberList extends Component {

	constructor(props) {
		super(props)
		this.state = {
			members: [],
			loading: false
		}
	}

	//mounting lifecyle component, runs right away when calling render on this component.
	componentDidMount() {  //lifecycle mounting, this will fire right away when
		this.setState({loading: true})	                     //component is rendered.
		fetch('https://api.randomuser.me/?nat=US&results=12')
			.then(response => response.json())
			.then(json => json.results)
			.then(members => this.setState({  //take the json results and set the state
				members,
				loading: false
			}))
	}

	render () {

		const { members, loading } = this.state

		return (
		<div className="member-list">
			<h1>Society Members</h1>

			{(loading) ?
				<span>loading...</span> :
				<span>{members.length} members</span>
			}

			{ (members.length ) ?
					members.map ( (member,i) =>
				<Member key={i}           //name and first are determined by the API (Randomuser.me
								name={member.name.first + ' ' + member.name.last}
								email={member.email}
								thumbnail={member.picture.thumbnail} />
					)	:
				<span>Currently 0 Members</span>
			}

		</div>
		)
	}
}

export default MemberList